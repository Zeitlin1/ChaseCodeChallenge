//
//  MapHandler.swift
//  ChaseCodeChallenge
//
//  Created by Anthony on 1/26/18.
//  Copyright © 2018 Anthony. All rights reserved.
//

import CoreLocation
import MapKit

// Mark: This Handles locating the device and sending the device's longitude and latitude to the viewcontroller to display on the map
class MapHandler: NSObject, CLLocationManagerDelegate {
    
    static let shared = MapHandler()
    
    // Mark: Optional Delegate used to transfer coordinates to ViewController
    var delegate: MapHandlerDelegate?
    
    var locationsArray = [[String: Any]]()
    
    let locationManager = CLLocationManager()
    
    var currentLat: Double?
    
    var currentLong: Double?
    
    override init(){
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
    }
    
     // Mark: Delegate function to handle location use permission
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
     // Mark: Delegate function to handle location updates on device
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        if let location = locations.first {
            
            currentLat = location.coordinate.latitude
            
            currentLong = location.coordinate.longitude
            
            delegate?.updateLocationCoordinates(lat: location.coordinate.latitude, long: location.coordinate.longitude)
        }
    }
    
     // Mark: Delegate function to handler location errors
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error)")
    }
}

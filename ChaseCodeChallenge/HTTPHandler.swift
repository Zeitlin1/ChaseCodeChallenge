//
//  HTTPHandler.swift
//  ChaseCodeChallenge
//
//  Created by Anthony on 1/26/18.
//  Copyright © 2018 Anthony. All rights reserved.
//

import Foundation

 // Mark: API Server Class.  Init as a singleton
class HTTPHandler {
    
    static let shared = HTTPHandler()
    
    let baseURL = "http://api.open-notify.org"
    
    let jsonParameter = "/iss-pass.json?"
    
     // Mark: private function to build and return fully assembled API call
    private func createURLRequest(lat: Double, long: Double) -> URLRequest {
        
        let urlString = baseURL + jsonParameter + "lat=\(lat)" + "&" + "lon=\(long)"
        
        let newURL = URL(string: urlString)
        
        let newURLRequst = URLRequest(url: newURL!)
        
        return newURLRequst
        
    }
    
     // Mark: Public function to send assembled URL Request to server
    public func requestInfoFor(lat: Double, long: Double, completionHandler: @escaping (Data?, HTTPURLResponse?, Error?) -> Void) {
        
        let urlReq = createURLRequest(lat: lat, long: long)
        
        URLSession.shared.dataTask(with: urlReq, completionHandler: { (data, response, error) in
            
             // Mark: Handle URL Request Error
            guard error == nil else {
                
                completionHandler(nil, nil, error)
                
                return
            }
            
            let httpResponse = response as! HTTPURLResponse
            
            let data = data
            
            completionHandler(data, httpResponse, nil)
            
        }).resume()
    }
}

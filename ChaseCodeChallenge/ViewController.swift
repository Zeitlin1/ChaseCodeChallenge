//
//  ViewController.swift
//  ChaseCodeChallenge
//
//  Created by Anthony on 1/26/18.
//  Copyright © 2018 Anthony. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {
    
    // Mark: Storyboard outlets
    @IBOutlet weak var mapViewOutlet: MKMapView!
    
    @IBOutlet weak var tableViewOutlet: UITableView!
    
    @IBOutlet weak var coordinatesLabel: UILabel!
    
    @IBOutlet weak var activityIndicatorOutlet: UIActivityIndicatorView!
    
    var sharedMap: MapHandler!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewOutlet.dataSource = self
        
        tableViewOutlet.delegate = self
        
        sharedMap = MapHandler.shared
        
        sharedMap.delegate = self
        
    }
    
}

    // Mark: Conforming to TableView subclass
extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sharedMapCount = sharedMap {
            return sharedMapCount.locationsArray.count
        } else {
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(Double(tableViewOutlet.frame.height) / Double(sharedMap.locationsArray.count))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCellID", for: indexPath) as! TableViewCell
        
        let cellInfo = sharedMap.locationsArray[indexPath.row]
        
        cell.numberLabel.text = "\(indexPath.row + 1)"

        // Mark: Optionally chain left and right label text to avoid null runtime errors
        if let leftText = cellInfo["duration"] as? Double {
            cell.leftLabel.text = String(leftText)
        }
        
        if let rightText = cellInfo["risetime"] as? Double {
            
            let date = Date(timeIntervalSince1970: rightText)
            
            let dayTimePeriodFormatter = DateFormatter()
            
            dayTimePeriodFormatter.dateFormat = "MMM dd YYYY hh:mm a"
            
            cell.rightLabel.text = dayTimePeriodFormatter.string(from: date)
        }
        
        return cell
    }
}

    // Mark: Conforming to MapHandlerDelegate protocol
extension ViewController: MapHandlerDelegate {
    
    func updateLocationCoordinates(lat: Double, long: Double) {
        
        activityIndicatorOutlet.startAnimating()
        
        HTTPHandler.shared.requestInfoFor(lat: lat, long: long) { (data, urlRes, error) in
            
            if let returnData = data {
                
                do {
                    
                    // Mark: Serializing JSON Response from api call to server
                    if let json = try JSONSerialization.jsonObject(with: returnData, options: []) as? [String: Any], let passDictionary = json["response"] as? [[String: Any]] {
                        
                        self.sharedMap.locationsArray.removeAll()
                        
                        for pass in passDictionary.enumerated() {
                        
                            print(pass.element)
                            
                            self.sharedMap.locationsArray.append(pass.element)
                        }
                        
                         // Mark: Optionally chaining latitude and longitude from device to avoid runtime error
                        if let lat = self.sharedMap.currentLat, let long = self.sharedMap.currentLong {
                            
                                let span = MKCoordinateSpanMake(0.1, 0.1)
                            
                                let userCoords = CLLocationCoordinate2D(latitude: lat, longitude: long)
                            
                                let region = MKCoordinateRegionMake(userCoords, span)
                                
                                self.mapViewOutlet.setRegion(region, animated: true)
                                
                        }
                        
                        // Mark: Modifying text labels, tableView and activity indicator on main thread asynchronously
                        DispatchQueue.main.async {
                            self.coordinatesLabel.text = "Latitude: \(lat) Longitude: \(long)"
                            
                            self.activityIndicatorOutlet.stopAnimating()
                            
                            self.tableViewOutlet.reloadData()
                        }
                    }
                    
                } catch {
                    // Mark: Handling thrown error from serialization attempt
                    print("Could not serialze JSON")
                }
            }
        }
    }
}

// Mark: MapHandlerDelegate protocol
protocol MapHandlerDelegate {
    func updateLocationCoordinates(lat: Double, long: Double)
}

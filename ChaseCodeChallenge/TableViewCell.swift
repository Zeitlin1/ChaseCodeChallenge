//
//  TableViewCell.swift
//  ChaseCodeChallenge
//
//  Created by Anthony on 1/26/18.
//  Copyright © 2018 Anthony. All rights reserved.
//

import Foundation
import UIKit

// Mark: Custom TableViewCell Class
class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var numberLabel: UILabel!
    
    @IBOutlet weak var leftLabel: UILabel!
    
    @IBOutlet weak var rightLabel: UILabel!
}
